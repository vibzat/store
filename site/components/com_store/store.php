<?php

if(!defined('KOOWA')) {
   JError::raiseWarning(0, JText::_("Koowa wasn't found. Please install the Koowa plugin and enable it."));
   return;
}

echo KService::get('com://site/store.dispatcher')->dispatch();
