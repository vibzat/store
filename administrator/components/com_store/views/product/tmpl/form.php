<?php
defined('KOOWA') or die('Restricted access');
?>
<?= @helper('behavior.tooltip') ?>
<?= @helper('behavior.validator') ?>
  
<script src="media://lib_koowa/js/koowa.js" />
<style src="media://lib_koowa/css/koowa.css" />

<form action="" method="post" id="product-form" class="-koowa-form" enctype="multipart/form-data">
   <input type="hidden" name="id" value="<?= $product->id; ?>" />

   <div style="width:100%; float: left" id="mainform">
      <fieldset class="adminform">
         <legend><?= @text('Details'); ?></legend>
         
         <table class="admintable">
            <tr>
               <td class="key">
                  <label for="category">
                     <?= @text( 'Category' ); ?>:
                  </label>
               </td>
               <td>
                  <?= @helper( 'listbox.categories', array('name' => 'store_category_id', 'selected' => $product->store_category_id, 'attribs' => array('class' => 'required')) ) ?>
               </td>
            </tr>
            <tr>
               <td class="key">
                  <label for="product_code">
                     <?= @text( 'Product Code' ); ?>:
                  </label>
               </td>
               <td>
                  <input class="product_code minLength:5" type="text" name="product_code" id="product_code" size="20" maxlength="32" value="<?= $product->product_code ?>" />
               </td>
            </tr>
            <tr>
               <td class="key">
                  <label for="name">
                     <?= @text( 'Product Name' ); ?>:
                  </label>
               </td>
               <td>
                  <input class="name minLength:5" type="text" name="name" id="name" size="60" maxlength="255" value="<?= $product->name ?>" />
               </td>
            </tr>
            <tr>
               <td class="key">
                  <label for="description">
                     <?= @text( 'Description' ); ?>:
                  </label>
               </td>
               <td>
                  <?= @editor( array(
                     'editor' => 'tinymce',
                     'name' => 'description',
                     'text' => $product->description, 
                     'height' => '391', 
                     'width' => '100%',
                     'cols' => '100', 
                     'rows' => '20', 
                     'buttons' => null, 
                     'options' => array('theme' => 'simple')
                  )); ?> 
               </td>
            </tr>
            <tr>
               <td class="key">
                  <label for="checkout_url">
                     <?= @text( 'Buy Link' ); ?>:
                  </label>
               </td>
               <td>
                  <input class="inputbox" type="text" name="checkout_url" id="checkout_url" size="90" value="<?= $product->checkout_url ?>" />
               </td>
            </tr>
            <tr>
               <td class="key">
                  <label for="featured">
                     <?= @text( 'Featured' ); ?>:
                  </label>
               </td>
               <td>
                 <?= @helper('select.booleanlist', array('name' => 'featured', 'selected' => $product->featured)); ?>
               </td>
            </tr>
            <tr>
               <td class="key">
                  <label for="published">
                     <?= @text( 'Published' ); ?>:
                  </label>
               </td>
               <td>
                 <?= @helper('select.booleanlist', array('name' => 'published', 'selected' => $product->published)); ?>
               </td>
            </tr>
         </table>
      </fieldset>
   </div>
</form>