<?php defined('KOOWA') or die; ?>

<form action="" method="get" class="-koowa-grid">
   <table class="adminlist"  style="clear: both;">
      <thead>
         <tr>
            <th width="20"></th>
            <th width="20"></th>
            <th width="250"><?= @helper('grid.sort', array('title'=>'Product Code', 'column'=>'product_code'))?></th>
            <th><?= @helper('grid.sort', array('column'=>'name'))?></th>
            <th width="250">
               <?= @helper('grid.sort', array('title'=>'Category', 'column'=>'category_name'))?>
            </th>
            <th width="30">
               <?= @helper('grid.sort', array('column' => 'featured')) ?>
            </th>
            <th width="30">
               <?= @helper('grid.sort', array('column' => 'published')) ?>
            </th>
            <th width="120">
               <?= @helper('grid.sort', array('title'=>'Created On', 'column'=>'created_on'))?>
            </th>
            <th width="120">
               <?= @helper('grid.sort', array('title'=>'Modified On', 'column'=>'modified_on'))?>
            </th>
         </tr>
         <tr>
            <td> </td>
            <td align="center"> 
               <?= @helper( 'grid.checkall'); ?>
            </td>
            <td>
               <?=@helper('grid.search');?> 
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
         </tr>
      </thead>

      <tbody>
         <?= @template('default_list')?>

         <? if (!count($products)): ?>
            <tr>
               <td colspan="20" align="center">
                  <?= @text('No items found'); ?>
               </td>
            </tr>
            <? endif; ?>

      </tbody>

      <tfoot>
         <tr>
            <td colspan="20">
               <?= @helper('paginator.pagination', array('total' => $total)) ?>
            </td>
         </tr>
      </tfoot>
   </table>
</form>
