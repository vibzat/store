<?php defined('KOOWA') or die; ?>

<? $i = 0;?>
<? foreach ($products as $product): ?>
<tr>
    <td align="center">
      <?= $product->id ?>
    </td>
    <td align="center">
        <?= @helper('grid.checkbox', array('row'=>$product)); ?>
    </td>
    <td align="left">
      <span class="editlinktip hasTip" title="<?= @text('Edit Product')?>">
         <a href="<?= @route('view=product&id='.$product->id); ?>">
            <?= $product->product_code ?>
         </a>
      </span>
    </td>
    <td align="left">
      <?= $product->name ?>
    </td>
    <td align="left">
      <?= $product->category_name ?>
    </td>
    <td align="center">
        <?= @helper('grid.enable', array('row' => $product, 'field' => 'featured')) ?>
    </td>
    <td align="center">
        <?= @helper('grid.enable', array('row' => $product, 'field' => 'published')) ?>
    </td>
    <td align="left">
       <?=@helper('date.humanize', array('date' => $product->created_on)); ?>
    </td>
    <td align="left">
       <?=@helper('date.humanize', array('date' => $product->modified_on)); ?>
    </td>
</tr>
<? ++$i?>
<? endforeach; ?>
