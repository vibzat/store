<?php
defined('KOOWA') or die('Restricted access');
?>
<?= @helper('behavior.tooltip') ?>
<?= @helper('behavior.validator') ?>
  
<script src="media://lib_koowa/js/koowa.js" />
<style src="media://lib_koowa/css/koowa.css" />

<form action="" method="post" id="category-form" class="-koowa-form">
   <input type="hidden" name="id" value="<?= $category->id; ?>" />

   <div style="width:60%; float: left" id="mainform">
      <fieldset class="adminform">
         <legend><?= @text('Details'); ?></legend>
         
         <table class="admintable">
            <tr>
               <td class="key">
                  <label for="name">
                     <?= @text( 'Name' ); ?>:
                  </label>
               </td>
               <td >
                  <input class="inputbox" type="text" name="name" id="name" size="60" maxlength="255" value="<?= $category->name; ?>" />
               </td>
            </tr>
            <tr>
               <td class="key">
                  <label for="description">
                     <?= @text( 'Description' ); ?>:
                  </label>
               </td>
               <td >
                  <textarea class="inputbox" name="description" id="description" rows="5" cols="45"><?= $category->description ?></textarea>
               </td>
            </tr>
            <tr>
               <td class="key">
                  <label for="published">
                     <?= @text( 'Published' ); ?>:
                  </label>
               </td>
               <td>
                 <?= @helper('select.booleanlist', array('name' => 'published', 'selected' => $category->published)); ?>
               </td>
            </tr>
         </table>
      </fieldset>
   </div>
</form>