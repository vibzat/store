<?php defined('KOOWA') or die; ?>

<? $i = 0;?>
<? foreach ($categories as $category): ?>
<tr>
    <td align="center">
      <?= $category->id ?>
    </td>
    <td align="center">
        <?= @helper('grid.checkbox', array('row'=>$category)); ?>
    </td>
    <td align="left">
      <span class="editlinktip hasTip" title="<?= @text('Edit Category')?>">
         <a href="<?= @route('view=category&id='.$category->id); ?>">
            <?= $category->name ?>
         </a>
      </span>
    </td>
    <td align="left">
      <?= substr($category->description, 0, 100) . ' ...' ?>
    </td>
    <td align="center">
        <?= @helper('grid.enable', array('row' => $category, 'field' => 'published')) ?>
    </td>
    <td align="center">
        <?= @helper('grid.order', array('row' => $category, 'total' => $total)) ?>
    </td>
    <td align="left">
       <?=@helper('date.humanize', array('date' => $category->created_on)); ?>
    </td>
    <td align="left">
       <?=@helper('date.humanize', array('date' => $category->modified_on)); ?>
    </td>
</tr>
<? ++$i?>
<? endforeach; ?>