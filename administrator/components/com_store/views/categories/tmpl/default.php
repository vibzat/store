<?php defined('KOOWA') or die; ?>

<form action="<?= @route()?>" method="get" class="-koowa-grid">
   <table class="adminlist"  style="clear: both;">
      <thead>
         <tr>
            <th width="20"></th>
            <th width="20"></th>
            <th><?= @helper('grid.sort', array('column'=>'name'))?></th>
            <th width="550">
               <?= @text('Description')?>
            </th>
            <th width="30">
               <?= @helper('grid.sort', array('column' => 'published')) ?>
            </th>
            <th width="40">
               <?= @helper('grid.sort', array('title' => 'Order', 'column' => 'ordering')) ?>
            </th>
            <th width="120">
               <?= @helper('grid.sort', array('title'=>'Created on', 'column'=>'created_on'))?>
            </th>
            <th width="120">
               <?= @helper('grid.sort', array('title'=>'Modified on', 'column'=>'modified_on'))?>
            </th>
         </tr>
         <tr>
            <td> </td>
            <td align="center"> 
               <?= @helper( 'grid.checkall'); ?>
            </td>
            <td>
               <?=@helper('grid.search');?> 
            </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
         </tr>
      </thead>

      <tbody>
         <?= @template('default_list')?>

         <? if (!count($categories)): ?>
            <tr>
               <td colspan="20" align="center">
                  <?= @text('No items found'); ?>
               </td>
            </tr>
            <? endif; ?>

      </tbody>

      <tfoot>
         <tr>
            <td colspan="20">
               <?= @helper('paginator.pagination', array('total' => $total)) ?>
            </td>
         </tr>
      </tfoot>
   </table>
</form>
