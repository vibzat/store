CREATE TABLE IF NOT EXISTS `#__store_categories` (
  `store_category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(120) DEFAULT NULL,
  `description` text,
  `published` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`store_category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__store_products` (
  `store_product_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `store_category_id` int(10) unsigned DEFAULT NULL,
  `uniqid` varchar(32) DEFAULT NULL,
  `product_code` varchar(32) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `price` decimal(10,2) DEFAULT NULL,
  `checkout_url` text,
  `featured` tinyint(1) DEFAULT NULL,
  `featured_start` date DEFAULT '2011-01-01',
  `featured_end` date DEFAULT '2100-01-01',
  `published` tinyint(1) DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`store_product_id`),
  UNIQUE KEY `store_product_id` (`store_product_id`),
  KEY `category_id` (`store_category_id`),
  KEY `featured` (`featured`),
  KEY `published` (`published`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;