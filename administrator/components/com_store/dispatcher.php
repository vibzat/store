<?php
defined('KOOWA') or die;

class ComStoreDispatcher extends ComDefaultDispatcher
{
    protected function _initialize(KConfig $config)
    {
        $config->append(array(
           //'request' => array('view' => KRequest::get('get.view', 'word', 'categories'))
           'controller' => 'categories'
        ));
        parent::_initialize($config);
    }
}