<?php
defined('KOOWA') or die('Restricted access');

class ComStoreTemplateHelperListbox extends ComDefaultTemplateHelperListbox
{
   public function categories( $config = array())
   {
      $config = new KConfig($config);
      $config->append(array(
         'model'      => 'categories',
         'name'       => 'store_category_id',
         'value'      => 'id',
         'text'      => 'name',
         'prompt'   => '- Select Category -',
         'attribs'    => array('id' => $config->name)
      ));

      return parent::_listbox($config);
   }
}
