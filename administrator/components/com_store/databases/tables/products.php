<?php
defined('KOOWA') or die('Restricted access');

class ComStoreDatabaseTableProducts extends KDatabaseTableAbstract
{
   
   public function _initialize(KConfig $config)
    {

        $config->append(array(
            'base'       => 'store_products',
            'behaviors'  => array('creatable', 'modifiable')
        ));

        parent::_initialize($config);
    }
   
}
