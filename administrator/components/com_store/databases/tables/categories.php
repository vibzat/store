<?php
defined('KOOWA') or die('Restricted access');

class ComStoreDatabaseTableCategories extends KDatabaseTableAbstract
{
   
   public function _initialize(KConfig $config)
    {

        $config->append(array(
            'base'       => 'store_categories',
            'behaviors'  => array('creatable', 'modifiable', 'orderable')
        ));

        parent::_initialize($config);
    }
   
}
