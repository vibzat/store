<?php
defined('KOOWA') or die('Restricted access');

class ComStoreModelCategories extends ComDefaultModelDefault
{
   public function __construct(KConfig $config)
   {
      parent::__construct($config);
   }

   public function getCategories()
   {
        $database = $this->getTable()->getDatabase();

      $query = $database->getQuery()
         ->distinct()
         ->from('store_categories AS tbl')
         ->order('created_on', 'DESC');

      $this->_buildQueryWhere($query);

      $result = $database->select($query, KDatabase::FETCH_FIELD_LIST);
      
      return $result;
   }

   protected function _buildQueryWhere(KDatabaseQuery $query)
   {
      $state = $this->_state;

      if($state->search)
      {
         $search = '%'.$state->search.'%';
         $query->where('name', 'LIKE',  $search)
            ->where('description', 'LIKE', $search, 'OR');
      }

      parent::_buildQueryWhere($query);
   }
}  
