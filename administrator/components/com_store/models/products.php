<?php
defined('KOOWA') or die('Restricted access');

class ComStoreModelProducts extends ComDefaultModelDefault
{
   public function __construct(KConfig $config)
   {
      parent::__construct($config);
   }

   public function getProducts()
   {
      $database = $this->getTable()->getDatabase();

      $query = $database->getQuery()
         ->distinct()
         ->from('store_products AS tbl')
         ->order('created_on', 'DESC');

      $this->_buildQueryWhere($query);

      $result = $database->select($query, KDatabase::FETCH_FIELD_LIST);
      
      return $result;
   }
   
   protected function _buildQueryColumns(KDatabaseQuery $query)
    {
        parent::_buildQueryColumns($query);

        $query->select('c.name AS category_name');
    }

    protected function _buildQueryJoins(KDatabaseQuery $query)
    {
         parent::_buildQueryJoins($query);
       
       $query->join('LEFT', 'store_categories AS c', 'c.store_category_id = tbl.store_category_id');
    }

   protected function _buildQueryWhere(KDatabaseQuery $query)
   {
      $state = $this->_state;

      if($state->search)
      {
         $search = '%'.$state->search.'%';
         $query->where('name', 'LIKE',  $search)
            ->where('description', 'LIKE', $search, 'OR');
      }

      parent::_buildQueryWhere($query);
   }
}  
